package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
)

func main() {
	if err := killServer("app.pid"); err != nil {
		fmt.Println("error:", err)
		if errors.Is(err, fs.ErrNotExist) {
			fmt.Printf("no such file\n")
		}
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Println(">", e)
		}
		os.Exit(1)
	}
}

func killServer(pidFile string) error {
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("warining: can't close %q - %s", pidFile, err)
		}
	}()

	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		// %w: wraps error, only valid in fmt.Errorf
		return fmt.Errorf("%q - bad pid: %w", pidFile, err)
	}

	fmt.Printf("killing process %d\n", pid)
	os.Remove(pidFile)

	return nil
}
