package main

import (
	"fmt"
	"strings"
)

func main() {
	players := []Player{
		{"Pugsley", 117},
		{"Wednesday", 5720},
	}

	// Value range semantics
	for _, p := range players {
		p.Score += 1000
	}
	fmt.Println(players)

	// Pointer range semantics
	for i := range players {
		players[i].Score += 1000
	}
	fmt.Println(players)
	// cart := []string{"lemon", "apple", "banana"}
	cart := []string{
		"lemon",
		"apple",
		"banana",
	}
	fmt.Println(cart, "len:", len(cart), "cap:", cap(cart))
	// for i := 0; i < len(cart); i++ { ... }
	for i := range cart {
		fmt.Println("i:", i, "val:", cart[i])
	}

	for i, val := range cart {
		fmt.Println("i:", i, "val:", val)
	}

	for _, val := range cart {
		fmt.Println("val:", val)
	}

	cart = append(cart, "bread")
	fmt.Println(cart, "len:", len(cart), "cap:", cap(cart))

	fruit := cart[:3] // slicing, half-open range
	fmt.Println(fruit, "len:", len(fruit), "cap:", cap(fruit))
	cart[0] = "pear"
	fmt.Println("cart:", cart)
	fmt.Println("fruit:", fruit)

	var values []int // nil
	for i := 0; i < 1000; i++ {
		values = appendInt(values, i)
	}
	fmt.Println(values[:5], values[len(values)-5:])

	/*
		data := make([]byte, 1_000_000) // read from file
		found := search([]byte{"hello"}, data)
	*/

	names := []string{ // Addams Family
		"Wednesday", "Pugsley", "Morticia", "Gomez",
	}
	// var lower []string
	lower := make([]string, 0, len(names))
	c := cap(lower)
	for _, name := range names {
		lower = append(lower, strings.ToLower(name))
		if newC := cap(lower); newC != c {
			fmt.Println(c, "->", newC)
			c = newC
		}
	}
	fmt.Println(lower)

	a := []int{1, 2, 3, 4}
	b := a[0:2:2] // avoid mutating a
	b = append(b, 100)
	fmt.Println("a:", a, "b:", b)

	x := []string{"A", "B"}
	y := []string{"C", "D", "E"}
	fmt.Println(concat(x, y)) // [A B C D E]
}

func concat(a, b []string) []string {
	// TODO: Your code goes here
	// Restriction: no "for" loops
	out := make([]string, len(a)+len(b))
	copy(out, a)
	copy(out[len(a):], b)
	return out
	// return append(a, b...)
}

func read() []byte {
	data := make([]byte, 1_000_000)
	return data[1000:1010]
}

func appendInt(values []int, value int) []int {
	i := len(values)
	// enough space in underlying array
	if len(values) < cap(values) {
		values = values[:len(values)+1]
	} else { // need to allocate and copy
		size := 2 * (len(values) + 1)
		fmt.Println(cap(values), "->", size)
		s := make([]int, size)
		copy(s, values)
		values = s[:len(values)+1]
	}

	values[i] = value
	return values
}

type Player struct {
	Name  string
	Score int
}
