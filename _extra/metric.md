# Metric

## Value Semantics

![](metric-val.png)

## Pointer Semantics (Stack)

![](metric-ptr-stack.png)

## Pointer Semantics (Heap)

![](metric-ptr-heap.png)
