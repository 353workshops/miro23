package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

/*
type Reader interface {
	Read(b []byte) (int, error)
}

type PyReader interface {
	Read(size int) ([]byte, error)
}

type Writer interface {
	Write(b []byte) (int, error)
}
*/

func main() {
	// GoLand "sha1/http.log.gz"
	sig, err := sha1Sum("http.log.gz")
	if err != nil {
		log.Fatalf("error: %s", err)
		/*
			fmt.Fprintf(os.Stderr, "error: %s\n", err)
			os.Exit(1)
		*/
	}
	fmt.Println(sig)

	sig, err = sha1Sum("sha1.go")
	if err != nil {
		log.Fatalf("error: %s", err)
		/*
			fmt.Fprintf(os.Stderr, "error: %s\n", err)
			os.Exit(1)
		*/
	}
	fmt.Println(sig)

	a, b := 1, 2
	{
		var a int
		a, b = 10, 20
		// a = 2
		fmt.Println("inner:", a, b)
	}
	fmt.Println("outer:", a, b)
}

// Exercise: Decompress only if fileName ends with .gz
// otherwise show the SHA1 of the uncompressed content
// Hint: strings.HasSuffix

// cat http.log.gz| gunzip | sha1sum
func sha1Sum(fileName string) (string, error) {
	// cat http.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		r, err = gzip.NewReader(file)
		// r, err := gzip.NewReader(file) // BUG
		// fmt.Printf("using gzip: %p\n", r)
		if err != nil {
			return "", err
		}
	}

	// | sha1sum
	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
