package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Millisecond)
	defer cancel()

	fmt.Println(userInfo(ctx, "ardanlabs"))
}

// Homework:
func userInfo(ctx context.Context, login string) (User, error) {
	url := fmt.Sprintf("https://api.github.com/users/%s", url.PathEscape(login))
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return User{}, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return User{}, err
	}
	// Status
	if resp.StatusCode != http.StatusOK {
		return User{}, fmt.Errorf("%q: bad status - %s", url, resp.Status)
	}
	defer resp.Body.Close()

	var u User
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&u); err != nil {
		return User{}, err
	}

	return u, nil
}

type User struct {
	Name     string
	NumRepos int `json:"public_repos"`
}

func decode() {
	file, err := os.Open("resp.json")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	var u User
	dec := json.NewDecoder(file)
	if err := dec.Decode(&u); err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Printf("%+v\n", u)
}

func get() {
	resp, err := http.Get("https://api.githubzzzzzzzzzzzzz.com/users/ardanlabs")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	// Status
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("error: bad status - %s", resp.Status)
	}
	defer resp.Body.Close()

	// Header
	fmt.Println(resp.Header.Get("Content-Type"))

	// Body
	io.Copy(os.Stdout, resp.Body)
}

/* encoding/json API
JSON -> []byte -> Go: json.Unmarshal
JSON -> io.Reader -> Go: json.NewDecoder
Go -> []byte -> JSON: json.Marshal
Go -> io.Writer -> JSON: json.NewEncoder

JSON <-> Go Types
string <-> string
null <-> nil
true/false <-> bool
number <-> [float64], float32, int, int8 ... int64, uint, uint8 ... uint64
array <-> []T, []any
object <-> map[string]any, struct {...}
*/
