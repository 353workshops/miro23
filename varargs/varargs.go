package main

import "fmt"

func main() {
	fmt.Println(sum())
	fmt.Println(sum(1, 2, 3))

	values := []int{1, 2, 3}
	fmt.Println(sum(values...))
}

func sum(values ...int) int {
	// values is a []int
	// fmt.Printf("%#v\n", values)
	total := 0
	for _, v := range values {
		total += v
	}
	return total
}
