package main

import (
	"context"
	"fmt"
	"runtime"
	"time"
)

func main() {
	// Show number of Ms
	fmt.Println("GOMAXPORCS:", runtime.GOMAXPROCS(0))

	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 3; i++ {
		// FIX 2: Use loop local variable
		i := i // shadows "i" from line 16
		go func() {
			fmt.Println("gr:", i) // use "i" from line 18
		}()
		/* FIX 1: Pass a parameter
		go func(n int) {
			fmt.Println("gr:", n)
		}(i)
		*/
		/* BUG: (closure) all goroutines use the same "i" from line 16
		go func() {
			fmt.Println("gr:", i)
		}()
		*/
	}

	time.Sleep(10 * time.Millisecond)

	ch := make(chan string)
	go func() {
		ch <- "hi" // send
	}()
	val := <-ch // receive
	fmt.Println(val)

	// producer
	go func() {
		defer close(ch)
		for i := 0; i < 3; i++ {
			ch <- fmt.Sprintf("message #%d", i)
		}
	}()

	for msg := range ch {
		fmt.Println(msg)
	}

	/* The range loop above does the following
	for {
		msg, ok := <-ch
		if !ok {
			break
		}
		fmt.Println(msg)
	}
	*/

	msg := <-ch // receive from a closed channel
	fmt.Printf("closed: %#v\n", msg)

	msg, ok := <-ch
	fmt.Printf("closed: %#v (ok=%v)\n", msg, ok)

	// ch <- "wassup?" // send to a closed channel (panic)

	var empty chan int
	go func() {
		fmt.Println("empty start")
		empty <- 3
		fmt.Println("empty end")
	}()
	time.Sleep(time.Millisecond)
	fmt.Println("main done")

	// buffered channel to avoid goroutine leak
	ch1 := make(chan int, 1)
	ch2 := make(chan int, 1)

	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- 1
	}()
	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- 2
	}()

	done := make(chan bool)
	go func() {
		time.Sleep(time.Millisecond)
		close(done)
	}()

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Printf("got %v from ch1\n", val)
	case val := <-ch2:
		fmt.Printf("got %v from ch2\n", val)
	case <-time.After(5 * time.Millisecond):
		fmt.Println("timeout")
		/*
			case <-done:
				fmt.Println("cancelled")
		*/
	case <-ctx.Done():
		fmt.Println("cancelled by context")
	}

	/*
		// drop packets
		select {
		case ch <- msg:
			// OK
		case <-time.After(time.Millisecond):
			// drop message
		}
	*/
}

/* channel semantics
- send/receive will block until opposite action¹
	[1] a channel with buffer "n" has "n" non-blocking sends
- receive from a closed channel will return the zero value for the channel type
- send to a closed channel will panic
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
*/

/* zero vs missing value problem:
- receive from a channel
- missing map key
- JSON (and other) serialization
*/
