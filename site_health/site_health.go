package main

import (
	"log"
	"net/http"
	"sync"
)

func main() {
	urls := []string{
		"https://go.dev",
		"https://pkg.go.dev",
		"https://example.com/no/such/path",
	}

	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, url := range urls {
		// wg.Add(1)
		url := url
		go func() { //u string) {
			defer wg.Done()
			reportHealth(url)
		}() //(url)
	}
	wg.Wait()
}

// Go 1.21
// GOEXPERIMENT=loopvar go run ./site_health

func reportHealth(url string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("error: %q - %s", url, err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		log.Printf("error: %q - bad status - %s", url, resp.Status)
		return
	}

	log.Printf("info: %q - OK", url)
}
