package main

import (
	"fmt"
)

func main() {
	loc := Location{}
	fmt.Printf("v %v\n", loc)
	fmt.Printf("+v %+v\n", loc)
	fmt.Printf("#v %#v\n", loc)

	// Aside: Use %#v for debug/log
	a, b := 1, "1"
	fmt.Printf("a=%v, b=%v\n", a, b)
	fmt.Printf("a=%#v, b=%#v\n", a, b)

	loc = Location{10, 20}
	fmt.Println("loc:", loc)
	loc = Location{
		// Y: 20,
		X: 10,
	}
	fmt.Println("loc:", loc)

	fmt.Println(NewLocation(10, 20))
	fmt.Println(NewLocation(10, 2000))

	loc1, err := NewLocation(10, 20)
	if err != nil {
		fmt.Println("can't create:", err)
	} else {
		fmt.Println("got:", loc1)
	}
	// loc2 := Location{-7, 10_000}

	p1 := Player{
		Name: "Parzival",
		//		X:        7.5,
		Location: Location{100, 200},
	}
	fmt.Println("p1:", p1)
	fmt.Println("p1.X:", p1.X)
	fmt.Println("p1.Location.X:", p1.Location.X)

	loc.Move(77, 88)
	fmt.Println("loc (move):", loc)

	p1.Move(400, 900)
	fmt.Println("p1 (move):", p1)

	ms := []Mover{
		&loc,
		&p1,
	}
	moveAll(ms, 0, 0)
	for _, m := range ms {
		fmt.Printf("moveAll:  %#v\n", m)
	}

	fmt.Printf("crystal: %v\n", Crystal)
	fmt.Printf("crystal (+): %+v\n", Crystal)
	// For #v implement the fmt.GoStringer interface.
	fmt.Printf("crystal (#): %#v\n", Crystal)

	p1.Found(Crystal)
	p1.Found(Crystal)
	fmt.Println(p1.Keys) // [crystal]

	k := Key(100)
	fmt.Println(k)
}

/* Exercise:
- Add a Keys slice to Player
- Add a Found(k Key) method to Player
	- It should add a specific key only once
*/

// String implements the fmt.Stringer interface.
func (k Key) String() string {
	switch k {
	case Crystal:
		return "crystal"
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	}

	return fmt.Sprintf("<Key %d>", k)
}

const (
	Copper Key = iota + 1
	Crystal
	Jade
)

type Key byte

// Rule of thumb: Return types, accept interfaces.

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

type Mover interface {
	Move(int, int)
	// Move(x, y int)
}

type Car struct {
	License string
	Location
}

func (p *Player) Found(k Key) {
	// if slices.Contains(p.Keys, k) { // Go version 1.21 +
	if p.hasKey(k) {
		return
	}
	p.Keys = append(p.Keys, k)
}

func (p *Player) hasKey(k Key) bool {
	for _, k2 := range p.Keys {
		if k2 == k {
			return true
		}
	}
	return false
}

type Player struct {
	Name string
	// X        float64
	Location // Player embeds Location
	// There's an implicit field called Location
	Keys []Key
}

// "l" is called: the reciever
// "l" is a pointer reciever
func (l *Location) Move(x, y int) {
	l.X = x
	l.Y = y
}

/*
func NewLocation(x, y int) Location
func NewLocation(x, y int) *Location
func NewLocation(x, y int) (Location, error)
*/
func NewLocation(x, y int) (*Location, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d of of range for %d/%d", x, y, maxX, maxY)
	}

	loc := Location{
		X: x,
		Y: y,
	}
	return &loc, nil
}

const (
	maxX = 1000
	maxY = 600
)

type Location struct {
	X int
	Y int
}

/* Thought experiment: Minimal interface for sorting

type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

func Sort(s Sortable) {
	...
}

*/
