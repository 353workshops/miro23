package main

import (
	"fmt"
)

func main() {
	m := NewMetric("cpu")
	fmt.Printf("%+v\n", m)
}

func NewMetric(name string) *Metric {
	// m := Metric{"cpu", 0}
	/*
		var m Metric
		m.Name = name
		m.Count = 0
	*/
	m := Metric{
		Name: name,
		//Count: 0,
	}
	// ...

	return &m
}

type Metric struct {
	Name  string
	Count int
}

// go build -gcflags=-m
