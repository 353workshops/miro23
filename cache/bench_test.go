package matlib

import (
	"testing"
)

const (
	rows = 1400
	cols = 1050
	/*
		rows = 20
		cols = 70
	*/
)

var (
	mat      [][]int
	n        = rows * cols
	expected = (n - 1) * n / 2 // sum of algebraic series
	sum      int
)

func init() {
	mat = make([][]int, rows)
	for r := range mat {
		mat[r] = make([]int, cols)
		for c := 0; c < cols; c++ {
			mat[r][c] = (r * cols) + c
		}
	}
}

func sumRows(mat [][]int) int {
	total := 0
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			total += mat[r][c]
		}
	}
	return total
}

func BenchmarkSumRows(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sum = sumRows(mat)
		if sum != expected {
			b.Fatal(sum)
		}
	}
}

func sumCols(mat [][]int) int {
	total := 0
	for c := 0; c < cols; c++ {
		for r := 0; r < rows; r++ {
			total += mat[r][c]
		}
	}
	return total
}

func BenchmarkSumCols(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sum = sumCols(mat)
		if sum != expected {
			b.Fatal(sum)
		}
	}
}
