package main

import "fmt"

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))

	/*
		size := 10
		a := make([]int, size)
		fmt.Println(a[11]) // will panic, no way to know at compile time
	*/
}

// named return values
func safeDiv(a, b int) (q int, err error) {
	// q & err are local variables in safeDiv
	// (just like a & b)

	defer func() {
		if e := recover(); e != nil {
			// fmt.Println("error:", e)
			err = fmt.Errorf("%s", e) // convert e (type any) to error
		}
	}()

	return div(a, b), nil
	/* Miki says: Don't! ☺
	q = div(a, b)
	return
	*/
}

func div(a, b int) int {
	return a / b
}
