package main

import "fmt"

func main() {
	/*
		var val int = 2
		var val = 2
		var val
		val = 2
	*/
	val := 2
	fmt.Printf("[main] val=%d at (%p)\n", val, &val)
	inc(&val)
	fmt.Printf("[main] val=%d at (%p)\n", val, &val)
}

func inc(val *int) {
	fmt.Printf("[inc ] val=%d at (%p)\n", *val, val)
	*val++
	fmt.Printf("[inc ] val=%d at (%p)\n", *val, val)
}

// go build -gcflags=-S > build.txt 2>&1
// int8, int16, int32, int64, int, uint8 (byte), uint16 ..., uint

// Get GC trace
// GODEBUG=gctrace=1 go run ./inc.go
