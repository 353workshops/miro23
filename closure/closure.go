package main

import "fmt"

func main() {
	add3 := makeAdder(3)
	fmt.Println(add3(7))

	add10 := makeAdder(10)
	fmt.Println(add10(7))
}

/* namespaces
add3 -> [(local) val: ] -> [(closure) n: ] -> [(package): fmt: ] -> [(builtin) len:]

/*
makeAddr [n = 3]
add3 n: ------^

makeAddr [n = 10]
add10 n: -----^
*/

func makeAdder(n int) func(int) int {
	fn := func(val int) int {
		return n + val
	}
	return fn
}
