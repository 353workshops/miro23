# Practical Go for Developers

ArdanLabs ∴  2023 <br />

Miki Tebeka
<i class="far fa-envelope"></i> [miki@ardanlabs.com](mailto:miki@ardanlabs.com), <i class="fab fa-twitter"></i> [@tebeka](https://twitter.com/tebeka), <i class="fab fa-linkedin-in"></i> [mikitebeka](https://www.linkedin.com/in/mikitebeka/), <i class="fab fa-blogger-b"></i> [blog](https://www.ardanlabs.com/blog/), <i class="fa-brands fa-medium"></i>[medium](https://medium.com/@tebeka).

[Syllabus](_extra/syllabus.pdf)

#### Shameless Plugs

- [Go Essential Training](https://www.linkedin.com/learning/go-essential-training/) - LinkedIn Learning
    - [Rest of classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Go Brain Teasers](https://pragprog.com/titles/d-gobrain/go-brain-teasers/) book


---

## Day 1: Efficient Code & Decoupling

### Agenda

- Mechanical sympathy: Writing efficient code
    - Data semantics
    - Memory allocations & the GC
    - Computer latency
- Working with slices
- Decoupling code
    - Structs, methods & interfaces
- A look into generics

### Code

- [game.go](game/game.go) - Structs & methods
- [varargs.go](varargs/varargs.go) - Variable arguments to functions
- [slices.go](slices/slices.go) - Working with slices
- [bench_test.go](cache/bench_test.go) - Cache friendly code
- [metric.go](metric/metric.go) - Heap allocation & escape analysis
- [inc.go](inc/inc.go) - Stack, value & pointers

### Links

- Slices
    - [Slices](https://go.dev/blog/slices) & [Slice internals](https://go.dev/blog/go-slices-usage-and-internals) on the Go blog
    - [Slice tricks](https://github.com/golang/go/wiki/SliceTricks)
- Strings
    - [Unicode table](https://unicode-table.com/)
    - [strings](https://pkg.go.dev/strings/) package - string utilities
    - [Go strings](https://go.dev/blog/strings)
- [Go Proverbs](https://go-proverbs.github.io/) - Think about them ☺
- [Annotated "Hello World"](https://www.353solutions.com/annotated-go)
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576)
- [Garbage Collection in Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
- [Greenspun's tenth rule](https://en.wikipedia.org/wiki/Greenspun%27s_tenth_rule)
- [Rob Pike's 5 Rules of Programming](https://users.ece.utexas.edu/~adnan/pike.html)
- [Effective Go](https://go.dev/doc/effective_go.html) - Read this!
- [Go standard library](https://pkg.go.dev/) - official documentation
- [Ultimate Go Tour](https://tour.ardanlabs.com/)
- Setting Up
    - [The Go SDK](https://go.dev/dl/)
    - [git](https://git-scm.com/)
    - IDE's: [Visual Studio Code](https://code.visualstudio.com/) + [Go extension](https://marketplace.visualstudio.com/items?itemName=golang.Go) or [Goland](https://www.jetbrains.com/go/) (paid)

### Data & Other

- `G☺`
- `♡`
- [Slides](_extra/slides.pdf)
- [Unicode](_extra/unicode.pdf)

---

## Day 2: Errors, I/O and HTTP

### Agenda

- Error handling
- I/O
    - The io.Reader & io.Writer interface
    - Working with files
    - Managing resources with defer
- Calling REST APIs
    - JSON Serialization

### Code

- [github.go](github/github.go) - Making HTTP requests
    - [resp.json](github/resp.json) - JSON response example
- [sha1.go](sha1/sha1.go) - Working with io.Reader & io.Writer
- [div.go](div/div.go) - Catching panics
- [kill_server.go](kill_server/kill_server.go) - Error handling
- [stats.go](stats/stats.go) - Intro to generics
- [any.go](any/any.go) - Working with the empty interface
- [game.go](game/game.go) - Interfaces

### Exercises

- Read and understand and examples in the [`sort` packages](https://pkg.go.dev/sort)
- Write `userInfo` in [github.go](github/github.go)

### Links

- [lensm](https://github.com/loov/lensm) - Show assembly next to source
- [guru](https://pkg.go.dev/golang.org/x/tools/cmd/guru) - See what interfaces are implemented by a type
- [Ardan Labs services repo](https://github.com/ardanlabs)
- [encoding/json](https://pkg.go.dev/encoding/json)
- [net/http](https://pkg.go.dev/net/http)
- [errors](https://pkg.go.dev/errors/) package ([Go 1.13](https://go.dev/blog/go1.13-errors))
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [Methods, interfaces & embedded types in Go](https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html)
- [Methods & Interfaces](https://go.dev/tour/methods/1) in the Go tour

### Data & Other

- [http.log.gz](_extra/http.log.gz)
- `https://api.github.com/users/ardanlabs`

---

## Day 3: Concurrency & Testing

### Agenda

- Concurrency with goroutines & channels
- Using context for timeouts and cancellations
- Testing your code

### Code


- testing
    - [nlp.go](nlp/nlp.go) - Tested code
    - [nlp_test.go](nlp/nlp_test.go) - Testing
    - [example_test.go](nlp/example_test.go) - Example tests
- [counter.go](counter/counter.go) - Using the race detector
- [github.go](github/github.go) - Adding context to HTTP calls
- [rtb.go](rtb/rtb.go) - Using context for timeout & cancellation
- [site_health.go](site_health/site_health.go) - Using sync.WaitGroup
- [dl_size.go](dl_size/dl_size.go) - Parallel download size
- [go_chan.go](go_chan/go_chan.go) - Goroutines & channels
- [closure.go](closure/closure.go) - Closure in Go

### Exercise:

Change `dl_size` to run only `n` goroutines at once.

### Links

- [Closure](https://en.wikipedia.org/wiki/Closure_(computer_programming)) on Wikipedia
- [conc](https://github.com/sourcegraph/conc) - High level concurrency
- [The race detector](https://go.dev/doc/articles/race_detector)
- [Uber Go Style Guide](https://github.com/uber-go/guide/blob/master/style.md)
- [conc](https://github.com/sourcegraph/conc) More concurrency primitives from SourceGraph
- [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup)
- [Data Race Patterns in Go](https://eng.uber.com/data-race-patterns-in-go/)
- [Go Concurrency Patterns: Pipelines and cancellation](https://go.dev/blog/pipelines)
- [Go Concurrency Patterns: Context](https://go.dev/blog/context)
- [Curious Channels](https://dave.cheney.net/2013/04/30/curious-channels)
- [The Behavior of Channels](https://www.ardanlabs.com/blog/2017/10/the-behavior-of-channels.html)
- [Channel Semantics](https://www.353solutions.com/channel-semantics)
- [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
- [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html)
- Testing
    - [testing](https://pkg.go.dev/testing/)
    - [testify](https://pkg.go.dev/github.com/stretchr/testify) - Many test utilities (including suites & mocking)
    - [Ginkgo](https://onsi.github.io/ginkgo/)
    - [Tutorial: Getting started with fuzzing](https://go.dev/doc/tutorial/fuzz)
        - [testing/quick](https://pkg.go.dev/testing/quick) - Initial fuzzing library
    - [test containers](https://golang.testcontainers.org/)

### Data & Other

- [nlp.go](_extra/nlp.go)
- [stemmer.go](_extra/stemmer.go)
- [tokenize_cases.yml](_extra/tokenize_cases.yml)

## Day 4

### Agenda

- Parsing command line with Cobra
- Managing dependencies
- Structuring your code
    - Writing sub-packages
- Writing an HTTP server
    - Writing handlers
    - Using chi for routing
Adding metrics & logging
    - Using expvar for metrics
    - Using the log package and a look at user/zap
- Configuration patterns
    - Reading environment variables and a look at external packages
    - Using the flag package for command line processing

### Code

- [dl_size.go](dl_size_pool/dl_size.go) - Limited number of goroutines
- [nlp](nlp) project


### Links

- [GoReleaser](https://goreleaser.com/) - Publish Go executables
- [Conway's Law](https://martinfowler.com/bliki/ConwaysLaw.html)
- [The Twelve-Factor App](https://12factor.net)
- Configuration
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
    - [viper](https://github.com/spf13/viper) & [cobra](https://github.com/spf13/cobra)
    - [conf](https://pkg.go.dev/github.com/ardanlabs/conf/v3)
- Logging 
    - Built-in [slog](https://pkg.go.dev/log/slog)
    - [uber/zap](https://pkg.go.dev/go.uber.org/zap)
    - [logrus](https://github.com/sirupsen/logrus)
    - The experimental [slog](https://pkg.go.dev/golang.org/x/exp/slog)
- Metrics
    - Built-in [expvar](https://pkg.go.dev/expvar/)
    - [Open Telemetry](https://opentelemetry.io/)
    - [Prometheus](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus)
- [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
- [Organizing Go Modules](https://go.dev/doc/modules/layout#server-project)
- [Example Project Structure](https://github.com/ardanlabs/service)
- [How to Write Go Code](https://go.dev/doc/code.html)
- Documentation
    - [Godoc: documenting Go code](https://go.dev/blog/godoc)
    - [Go Doc Comments](https://go.dev/doc/comment)
    - [Testable examples in Go](https://go.dev/blog/examples)
    - [Go documentation tricks](https://godoc.org/github.com/fluhus/godoc-tricks)
    - [gob/doc.go](https://github.com/golang/go/blob/master/src/encoding/gob/doc.go) of the `gob` package. Generates [this documentation](https://pkg.go.dev/encoding/gob/)
    - `go install golang.org/x/pkgsite/cmd/pkgsite@latest` (require go 1.18+)
    - `pkgsite -http=:8080` (open browser on http://localhost:8080/${module name})
- [Out Software Dependency Problem](https://research.swtch.com/deps) - Good read on dependencies by Russ Cox
- Linters (static analysis)
    - [staticcheck](https://staticcheck.io/)
    - [golangci-lint](https://golangci-lint.run/)
    - [gosec](https://github.com/securego/gosec) - Security oriented
    - [vulncheck](https://pkg.go.dev/golang.org/x/vuln/vulncheck) - Check for CVEs
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis) - Helpers to write analysis tools (see [example](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/))
- HTTP Servers
    - [net/http](https://pkg.go.dev/net/http/)
    - [net/http/httptest](https://pkg.go.dev/net/http/httptest)
    - [gorilla/mux](https://github.com/gorilla/mux) - HTTP router with more frills
    - [chi](https://github.com/go-chi/chi) - A nice web framework

### Data & Other

- [nlp.go](_extra/nlp.go)
- [stemmer.go](_extra/stemmer.go)
- [tokenize_cases.yml](_extra/tokenize_cases.yml)
