package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	// var mu sync.Mutex
	// counter := 0
	counter := int64(0)

	g := 10
	var wg sync.WaitGroup
	wg.Add(g)

	for i := 0; i < g; i++ {
		go func() {
			defer wg.Done()
			for i := 0; i < 1000; i++ {
				time.Sleep(time.Microsecond)
				// mu.Lock()
				// counter++
				// mu.Unlock()
				atomic.AddInt64(&counter, 1)
			}
		}()
	}

	wg.Wait()
	fmt.Println(counter)
}
