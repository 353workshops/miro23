package nlp

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTokenize(t *testing.T) {
	text := "What's on second?"
	expected := []string{"what", "on", "second"}
	tokens := Tokenize(text)
	/*
		if !reflect.DeepEqual(expected, tokens) {
			t.Fatalf("expected: %#v, got #%v", expected, tokens)
		}
	*/
	require.Equal(t, expected, tokens)

	t.Log("HERE")
}

var tokenizeCases = []struct {
	text   string
	tokens []string
}{
	{"Who's on first?", []string{"who", "on", "first"}},
	{"", nil},
}

func TestTokenizeTable(t *testing.T) {
	setup(t)
	defer teardown(t)
	t.Cleanup(func() { teardown(t) })

	for _, tc := range tokenizeCases {
		name := tc.text
		if name == "" {
			name = "<empty>"
		}
		t.Run(name, func(t *testing.T) {
			tokens := Tokenize(tc.text)
			require.Equal(t, tc.tokens, tokens)

		})
	}
}

func isCI() bool {
	return os.Getenv("CI") != "" || os.Getenv("BUILD_NUMBER") != ""
}

func TestCI(t *testing.T) {
	if !isCI() {
		t.Skip("not in CI")
	}
	t.Log("running CI test")
}

func setup(t *testing.T) {
	/*
		db := startDB()
		t.Cleanup(db.Close())
		return db
	*/
}
func teardown(t *testing.T) {}

// TODO: Mock interface
