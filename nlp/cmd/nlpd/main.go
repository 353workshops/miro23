package main

import (
	"context"
	_ "embed"
	"encoding/json"
	"errors"
	"expvar"
	"fmt"
	"io"
	"log"
	"log/slog"
	"net/http"
	"net/http/pprof"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/spf13/cobra"

	"github.com/ardanlabs/nlp"
	"github.com/ardanlabs/nlp/stemmer"
)

// defaults < configuration file < environment < command line

var (
	//go:embed version.txt
	version string

	versionMetric = expvar.NewString("version")
	tokErrsMetric = expvar.NewInt("tokenize.errors")

	options struct {
		port int
	}
)

func main() {
	cmd.Flags().IntVarP(&options.port, "port", "p", 8080, "port to listen on")

	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}

var cmd = cobra.Command{
	Use:   "nlpd",
	Short: "nlp HTTP server",
	Run:   run,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) > 0 {
			return errors.New("no arguments expected")
		}

		if options.port < 0 || options.port > 65_535 {
			return fmt.Errorf("%d - invalid port", options.port)
		}

		return nil
	},
}

func run(cmd *cobra.Command, args []string) {
	versionMetric.Set(version)
	// fmt.Println("version:", version)
	/*
		http.HandleFunc("/health", healthHandler)
		http.HandleFunc("/tokenize", tokenizeHandler)
	*/
	/* net/http Routing:
	- end with / - prefix match
	- otherwise exact match

	In 1.22
	"GET /users/{id}"
	*/

	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))
	api := API{
		logger: logger.With("app", "nlpd"),
	}

	mux := chi.NewMux()
	mux.Get("/health", api.healthHandler)
	mux.Post("/tokenize", api.tokenizeHandler)
	mux.Get("/stem/{word}", api.stemHandler)

	mux.Handle("/debug/vars", expvar.Handler())
	mux.Get("/debug/pprof/profile", pprof.Profile)

	srv := http.Server{
		Handler: RequestIDMiddleware(mux),
		Addr:    fmt.Sprintf(":%d", options.port),
	}

	api.logger.Info("server starting", "adderss", srv.Addr)
	if err := srv.ListenAndServe(); err != nil {
		log.Fatalf("error: %s", err)
	}
}

type ctxKeyType string

var idKey ctxKeyType = "id"

func RequestIDMiddleware(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// Before handler
		id := uuid.NewString()
		ctx := context.WithValue(r.Context(), idKey, id)
		r = r.Clone(ctx)

		// Call original handler
		h.ServeHTTP(w, r)

		// Post call
	}

	return http.HandlerFunc(fn)
}

type API struct {
	logger *slog.Logger
}

func (a *API) stemHandler(w http.ResponseWriter, r *http.Request) {
	a.logger.Info("request id", "id", r.Context().Value(idKey))
	word := chi.URLParam(r, "word")
	if word == "" {
		http.Error(w, "no word", http.StatusBadRequest)
		return
	}

	stem := stemmer.Stem(word)
	fmt.Fprintln(w, stem)

}

func (a *API) tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	/* pre chi
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/
	// GET + PARSE
	rdr := http.MaxBytesReader(w, r.Body, 1_000_000)
	data, err := io.ReadAll(rdr)
	if err != nil {
		tokErrsMetric.Add(1)
		a.logger.Error("can't read", "error", err, "remote", r.RemoteAddr)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}
	text := string(data)

	// WORK
	tokens := nlp.Tokenize(text)

	// ENCODE + OUTPUT
	w.Header().Set("content-type", "application/json")
	out := map[string]any{
		"tokens": tokens,
	}
	json.NewEncoder(w).Encode(out)
}

func (a *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	/* pre chi
	if r.Method != http.MethodGet {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/
	fmt.Fprintf(w, "OK (version=%s)", version)
}
