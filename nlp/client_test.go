package nlp

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

// Mock
type errTransport struct{}

func (t errTransport) RoundTrip(*http.Request) (*http.Response, error) {
	return nil, fmt.Errorf("can't connect")

}

func TestHealthError(t *testing.T) {
	c := Client{
		baseURL: "https://example.com",
	}
	c.c.Transport = errTransport{}
	err := c.Health(context.Background())
	require.Error(t, err)

}
