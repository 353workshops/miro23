package main

import "fmt"

func main() {
	var a any // interface{} (go < 1.18)
	a = 7
	fmt.Printf("%v of %T\n", a, a)

	a = "Hi"
	fmt.Printf("%v of %T\n", a, a)

	s := a.(string) // type assertion
	fmt.Println(s)

	// i := a.(int) // will panic
	i, ok := a.(int)
	if ok {
		fmt.Println(i)
	} else {
		fmt.Println("not an int")
	}

	/*
		a, b := 1, 1.0
		fmt.Println(a + b) // won't compile
	*/

	/*
		s1 := string(62)
		fmt.Println(s1) // >
	*/

	// See also "type switch"
}
