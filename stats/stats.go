package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(Max[int](nil))
	fmt.Println(Max([]int{1, 2, 3}))
	fmt.Println(Max([]float64{1, 2, 3}))
	fmt.Println(Max([]time.Month{1, 2, 3}))
	fmt.Println(Max([]string{"A", "B", "C"}))
}

type Ordered interface {
	~int | ~float64 | ~string
}

func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		var zero T // var zero int = 0
		return zero, fmt.Errorf("Max of empty slice")
	}

	max := values[0]
	for _, val := range values[1:] {
		if val > max {
			max = val
		}
	}
	return max, nil
}

/*
func MaxInts(values []int) (int, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("MaxInts of empty slice")
	}

	max := values[0]
	for _, val := range values[1:] {
		if val > max {
			max = val
		}
	}
	return max, nil
}

func MaxFloat64s(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("MaxFloat64s of empty slice")
	}

	max := values[0]
	for _, val := range values[1:] {
		if val > max {
			max = val
		}
	}
	return max, nil
}

*/
