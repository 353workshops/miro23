package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

/*
Solve the following exercise:
The TLC Trip Record Data has data files in the following URL format (2 per month):
https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_2023-03.parquet
https://d37ci6vzurychx.cloudfront.net/trip-data/green_tripdata_2023-03.parquet

Write a function

func downloadSize(startDate, endDate time.Time) (int64, error)

that will return the download size in megabytes of all the parquet files in this date range.

Use an HTTP HEAD request to get the file size
Process each file in a different goroutine
*/

// Exercise: Limit the number of gorouintes to "n"

func main() {
	// fmt.Println(dlSize("https://d37ci6vzurychx.cloudfront.net/trip-data/green_tripdata_2023-03.parquet"))

	start := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(2022, 12, 1, 0, 0, 0, 0, time.UTC)
	colors := []string{"green", "yellow"}
	dates := dateRange(start, end)

	pool := make(chan int, 4) // allow only 4 goroutines at once
	// The other option is to have worker pool
	t1 := time.Now()
	total := int64(0)
	ch := make(chan result)
	// fan out
	for _, t := range dates {
		for _, color := range colors {
			month := t.Format("2006-01")
			url := fmt.Sprintf("https://d37ci6vzurychx.cloudfront.net/trip-data/%s_tripdata_%s.parquet", color, month)
			go func() {
				pool <- 1                 // acquire lock
				defer func() { <-pool }() // release lock

				r := result{url: url}
				r.size, r.err = dlSize(url)
				ch <- r
			}()
		}
	}

	// collect
	for i := 0; i < len(dates)*len(colors); i++ {
		r := <-ch
		if r.err != nil {
			log.Fatalf("error: %q - %s", r.url, r.err)
		}
		total += r.size
	}

	fmt.Println(total)
	fmt.Println(time.Since(t1))
}

/*
sequential:
314573299
1.799s
concurrent:
0.929s
*/

type result struct {
	url  string
	err  error
	size int64
}

func dateRange(start, end time.Time) []time.Time {
	var out []time.Time
	for t := start; !t.After(end); t = t.AddDate(0, 1, 0) {
		out = append(out, t)
	}
	return out
}

func dlSize(url string) (int64, error) {
	resp, err := http.Head(url)
	if err != nil {
		return 0, err
	}
	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("%q - bad status: %s", url, resp.Status)
	}

	return resp.ContentLength, nil
}
